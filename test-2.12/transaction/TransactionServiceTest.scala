package transaction

import org.specs2.mutable.Specification
import play.api.inject.guice.GuiceInjectorBuilder
import play.api.inject.{Injector, bind}

/**
  * Created by kiran on 5/11/17
  */

class TransactionServiceTest extends Specification {

  val injector: Injector = new GuiceInjectorBuilder()
          .bindings(bind[TransactionRepository].to[InMemoryTransactionRepository])
          .injector

  "TransactionServiceTest#save" should {
    "save" in {
      val transactionService = injector.instanceOf[TransactionService]
      transactionService.clearAll()
      val t = Transaction(Some(123), 456.7, "bills")
      val saved = transactionService.save(999, t)
      saved.isRight must equalTo(true)
      saved.right.get.parentId must beEqualTo(t.parentId)
    }
  }

  "TransactionServiceTest#save" should {
    "return err" in {
      val transactionService = injector.instanceOf[TransactionService]
      transactionService.clearAll()
      val t = Transaction(Some(123), 456.7, "bills")
      val saved = transactionService.save(999, t)
      val t2 = Transaction(Some(123), 32432, "grocery")
      val saved2 = transactionService.save(999, t2)
      saved2.isLeft must equalTo(true)
    }
  }

  "TransactionServiceTest#find" should {
    "find" in {
      val transactionService = injector.instanceOf[TransactionService]
      transactionService.clearAll()
      val t = Transaction(Some(123), 456.7, "bills")
      val saved = transactionService.save(999, t)

      transactionService.find(999).isRight must equalTo(true)
      transactionService.find(998).isLeft must equalTo(true)
    }
  }


  "TransactionServiceTest#findAllOfCategory" should {
    "find all in category" in {
      val transactionService = injector.instanceOf[TransactionService]
      transactionService.clearAll()
      transactionService.save(1, Transaction(None, 456.7, "bills"))
      transactionService.save(2, Transaction(Some(1), 23.2, "bills"))
      transactionService.save(3, Transaction(None, 33.2, "shopping"))
      transactionService.save(4, Transaction(Some(1), 49, "bills"))
      transactionService.save(5, Transaction(Some(3), 4, "shopping"))

      transactionService.findAllOfCategory("bills") must contain(allOf(1L, 2L, 4L))
      transactionService.findAllOfCategory("shopping") must contain(allOf(3L, 5L))
      transactionService.findAllOfCategory("grocery") must haveSize(0)
    }
  }

  "TransactionService#sumAll" should {
    "sum all related tx" in {
      val transactionService = injector.instanceOf[TransactionService]
      transactionService.clearAll()
      transactionService.save(1, Transaction(None, 456.7, "bills"))
      transactionService.save(2, Transaction(Some(1), 23.2, "bills"))
      transactionService.save(3, Transaction(None, 33.2, "shopping"))
      transactionService.save(4, Transaction(Some(1), 49, "bills"))
      transactionService.save(5, Transaction(Some(3), 4, "shopping"))

      transactionService.sumAll(1).isRight must equalTo(true)
      transactionService.sumAll(1).right.get must equalTo(456.7 + 23.2 + 49)
      transactionService.sumAll(2).isRight must equalTo(true)
      transactionService.sumAll(2).right.get must equalTo(23.2)
      transactionService.sumAll(3).isRight must equalTo(true)
      transactionService.sumAll(3).right.get must equalTo(33.2 + 4)
      transactionService.sumAll(9).isLeft must equalTo(true)
    }
  }
}
