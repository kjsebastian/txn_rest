package transaction

import javax.inject.Singleton

import play.api.Logger

import scala.collection.mutable

/**
  * Created by kiran on 5/11/17
  */

trait TransactionRepository {
  def save(id: Long, transaction: Transaction): Transaction
  def find(id: Long): Option[Transaction]
  def findAll(category: String): Seq[Long]
  def findAll(id: Long): Seq[Transaction]
  def clearAll(): Unit
}

@Singleton
class InMemoryTransactionRepository extends TransactionRepository{

  private val db  = mutable.Map[Long, Transaction]()

  override def save(id: Long, transaction: Transaction): Transaction = {
    db.put(id, transaction)
    db(id)
  }

  override def find(id: Long): Option[Transaction] = {
    db.get(id)
  }

  override def findAll(category: String): Seq[Long] = {
    Logger.info(s"$db")
    db.filter { case (_, t) => t.category == category }.keys.toSeq
  }

  override def findAll(parentId: Long): Seq[Transaction] = {
    db.values.filter(t => t.parentId.contains(parentId)).toSeq
  }

  def clearAll(): Unit = {
    db.clear()
  }
}
