package transaction

import javax.inject.{Inject, Singleton}

import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}

/**
  * Created by kiran on 5/11/17
  */

@Singleton
class TransactionController @Inject()(cc: ControllerComponents,
                                      transactionService: TransactionService) extends AbstractController(cc) {

  private val log = Logger(getClass)

  def save(id: Long) = Action(parse.json[Transaction]) { implicit request =>
    val t = request.body
    transactionService.save(id, t) match {
      case Left(err) => BadRequest(err)
      case Right(tx) => Ok(Json.toJson(tx))
    }
  }

  def find(id: Long) = Action {
    transactionService.find(id) match {
      case Left(err) => BadRequest(err)
      case Right(t) => Ok(Json.toJson(t))
    }
  }

  def findAllOfCategory(category: String) = Action {
    Ok(Json.toJson(transactionService.findAllOfCategory(category)))
  }

  def sumAll(id: Long) = Action {
    transactionService.sumAll(id) match {
      case Left(err) => BadRequest(err)
      case Right(total) => Ok(Json.toJson(Sum(total)))
    }
  }
}
