package transaction

import play.api.libs.json._
import play.api.libs.functional.syntax._

/**
  * Created by kiran on 5/11/17
  */

case class Transaction (parentId: Option[Long], amount: Double, category: String)

object Transaction {
  implicit val txnWrites: Writes[Transaction] = (t: Transaction) => Json.obj(
    "parent_id" -> t.parentId,
    "amount" -> t.amount,
    "type" -> t.category
  )

  implicit val txnReads: Reads[Transaction] = (
    (__ \ "parent_id").readNullable[Long] and
      (__ \ "amount").read[Double] and
      (__ \ "type").read[String]
  )(Transaction.apply _)
}

case class Sum(sum: Double)

object Sum {
  implicit val sumWrites: OWrites[Sum] = Json.writes[Sum]
  implicit val sumReads: Reads[Sum] = Json.reads[Sum]
}

