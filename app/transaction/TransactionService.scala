package transaction

import javax.inject.{Inject, Singleton}

/**
  * Created by kiran on 5/11/17
  */

@Singleton
class TransactionService @Inject()(repo: TransactionRepository) {

  def save(id: Long, transaction: Transaction): Either[String, Transaction] = {
    repo.find(id) match {
      case Some(_) => Left(s"transaction with id $id already exists")
      case None => Right(repo.save(id, transaction))
    }
  }

  def find(id: Long): Either[String, Transaction] = {
    repo.find(id) match {
      case Some(t) => Right(t)
      case None => Left("transaction not found")
    }
  }

  def findAllOfCategory(category: String): Seq[Long] = {
    repo.findAll(category: String)
  }

  def sumAll(id: Long): Either[String, Double] = {
    repo.find(id) match {
      case Some(t) => {
        val sumChildren = repo.findAll(id).map(tx => tx.amount).sum
        Right(sumChildren + t.amount)
      }
      case None => Left("transaction not found")
    }
  }

  def clearAll(): Unit = {
    repo.clearAll()
  }
}
